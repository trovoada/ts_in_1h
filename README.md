# README #
Welcome to TypeScript in 1h

### What is this repository for? ###

* This repository will help on the workshop
* 1.0

### How do I get set up? ###


* Node.js and TypeScript should be installed
	
	https://nodejs.org/
	
	After installed, typescript should be installed  with:
		* npm install -g typescript  - to install globally
		* npm install - to install only on the project scope
		* npm start to compile the file to js
		* npm run watch to run in watch mode
		
	* compile ts and generate js:
		* tsc filename
	* compile and run in  watch mode
		* tsc filename.ts -w


### Contribution guidelines ###


* Homilzio Trovoada Santos

### Who do I talk to? ###

* Homilzio Trovoada Santos
* homilzio.santos@celfocus.com
