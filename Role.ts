export enum Role {
    everyone = 0,
    BeachUser = 1,
    Surfer = 2,
    LifeGuard = 3
  }
  