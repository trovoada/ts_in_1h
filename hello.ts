import { Role } from "./Role";

const city : string = 'Lisbon';
const beachLimit : number = 17000;
const canAccessBeach : boolean = true;
let index = city.indexOf('b');
const address: any = { street: '', city: ''};
const blueFlagBeaches : string [] = [''];

blueFlagBeaches.push('Foz do Arelho', 'Carcavelos');

//blueFlagBeaches.push(1);
//blueFlagBeaches.push(true);

//blueFlagBeaches = [17, 14, 10];


function checkInAmountIntoBeach(amount: number, beachName : string, canCheckIn: boolean){
  alert("checkInAmountIntoBeach called from TS");
  return canCheckIn && addAmount(amount, beachName);
}

const addAmount = (amount: number, beachName : string)=> {
  return amount <= 10 && blueFlagBeaches.indexOf(beachName) > -1;
}
let amount : boolean = addAmount(2, 'Costa');
//let amountNumber : number = addAmount(2, 'Costa');

let isValidAmount = checkInAmountIntoBeach(2, 'Comporta', true);
//let isNotValidAmount = checkInAmountIntoBeach(2, 'Comporta');

interface Beach {
  name : string;
  lotation : number;
  hasBlueFlag: boolean;
}

// TODO add more beaches to show the alert of interface
class Carcavelos implements Beach{
    name: string;
    lotation: number;
    hasBlueFlag: boolean;
    address: any;

    public constructor(name: string, lotation: number, hasBlueFlag: boolean ){
        this.name = name;
        this.lotation = lotation;
        this.hasBlueFlag = hasBlueFlag;
    }

    public canAccess(role:number, name : string) {
      return Role.everyone < role && name !== 'Trump';
    }

    /*
    public constructor(private name: string, private lotation: number, private hasBlueFlag: boolean ){
        this.name = name;
        this.lotation = lotation;
        this.hasBlueFlag = hasBlueFlag;
    }*/

}

// TS vs JS
const message: string = 'hello world';
console.log(message);

"use strict";
const newMessage = 'hello world';
console.log(message);